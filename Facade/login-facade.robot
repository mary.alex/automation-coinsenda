*** Settings ***
Resource       ../../automation-coinsendaconfiguration/configuration-dev.robot

*** Variables ***
${email}  id:email
${password}  id:password
${button-SignIn}  Xpath://*[@id="authForm"]/div[6]/div/div

*** Keywords ***
Login as user
   I am in coinsenda login page
   I fill the email 
   I fill the password
   I click the Sign In button

I am in coinsenda login page
   Open Browser   ${URL_LOGIN}  chrome

I fill the email
   Wait Until Element is Visible   ${email}  30
   Input text   ${email}    ${EMAIL}
  
I fill the password
   Wait Until Element is Visible   ${password}  30
   Input text   ${password}    ${ADMIN}
    
I click the Sign In button
   click element  ${button-SignIn} 