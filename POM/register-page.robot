*** Settings ***

*** Variables ***
${createAccountButton}   Xpath://*[@id="containerElement"]/div[1]/div[3]/div[1]/div[2]/label
${registerText}   Xpath://*[@id="app"]/section/div/div[3]/div[1]/div/p
${urlText}   Xpath://*[@id="app"]/section/div/div[3]/div[2]/div/div/div/div/div[1]/img
${googleButton}   Xpath://*[@id="authForm"]/div[2]/div[2]
${loginLink}  Xpath://*[@id="app"]/section/div/div[3]/div[2]/div/p/b
${emailNewAccount}   id:email
${passlNewAccount}   id:password
${pass2lNewAccount}   id:password2
${terms}   Xpath://*[@id="authForm"]/div[2]/div[7]
${registerButton}    Xpath://*[@id="authForm"]/div[2]/div[8]/div
${messageCreate}   Xpath://*[@id="authForm"]/div[2]/div[7]/div
${refCode}    id="ref_code"
${loginbutton}  Xpath://*[@id="containerElement"]/div[1]/div[3]/div[1]/div[1]
${registerAccountLink}   Xpath://*[@id="authForm"]/p/b
${startButton}   Xpath://*[@id="home"]/div/div/div[2]/button