***Settings***
Library  Selenium2Library

Resource        ../../automation-coinsenda/stepdefinitions/register_components_sd.robot

***Test Cases***
Check elements on register page
   Given that the coinsenda register page is open
   Then the content load correctly 

*** Keywords ***
that the coinsenda register page is open
   Given I am in coinsenda page
   Then press create account button

the content load correctly 
   Then the register text is visible 
   And the Coinsenda url text is visible
   And the regiter with google button is visible  
   And the login link is visible   