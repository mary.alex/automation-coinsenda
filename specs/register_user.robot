***Settings***
Library  Selenium2Library
Library  Dialog 

Resource       ../stepdefinitions/register_components_sd.robot
Resource       ../stepdefinitions/register_user_sd.robot

***Test Cases***
Regiter a new account
   Given that the coinsenda register page is open
   When the data is filled for new account
   Then the new account was created 

Regiter a new account with referent code
   Given that the coinsenda register page is open
   When the data is filled for new account
   And enter the referent code
   Then the new account was created 

Register a new account from login
   Given that the coinsenda page is open
   when press register button from form page
   And the data is filled for new account
   Then the new account was created 

Register a new account from start now button
   Given that the coinsenda page is open
   when press start now button
   And the data is filled for new account
   Then the new account was created 

*** Keywords ***
that the coinsenda register page is open
   Given I am in coinsenda page
   Then press create account button

the data is filled for new account
   Given enter the email
   When enter the password
   And enter the confirmation password
   And accept the terms and conditions 

the new account was created
   Given press register button
   When Execute Manual Step
   Then check the confirmation message

enter the referent code
   Given enter the referent code   

that the coinsenda page is open
   Given I am in coinsenda page
   
press register button from form page
   Given press login button
   Then press new account link

press start now button
   Given press start button