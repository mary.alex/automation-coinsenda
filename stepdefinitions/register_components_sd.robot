*** Settings ***
Resource      ../../automation-coinsenda/configuration/configuration-dev.robot
Resource      ../../automation-coinsenda/POM/register-page.robot

*** Keywords ***

Given I am in coinsenda page
    Open Browser    ${URL_LOGIN}    chrome

press create account button
    Wait Until Element is Visible   ${createAccountButton}  10
    Click element    ${createAccountButton}
    
the register text is visible
    Wait Until Element is Visible   ${registertext}   10
    
the Coinsenda url text is visible  
    Wait Until Element is Visible   ${urlText}  10
    
the regiter with google button is visible
    Wait Until Element is Visible   ${googleButton}  10
    
the login link is visible
    Wait Until Element is Visible   ${loginLink}  10      