*** Settings ***
Resource      ../../automation-coinsenda/configuration/configuration-dev.robot
Resource      ../../automation-coinsenda/POM/register-page.robot

*** Keywords ***
enter the email
  Wait Until Element is Visible   ${emailNewAccount}  10
  Input text   ${emailNewAccount}    prueba11.mora@gmail.com

enter the password
  Wait Until Element is Visible   ${passlNewAccount}  10
  Input text   ${passlNewAccount}    prueba123

enter the confirmation password
  Wait Until Element is Visible   ${pass2lNewAccount}  10
  Input text   ${pass2lNewAccount}    prueba123

accept the terms and conditions
   Wait Until Element is Enabled   ${terms}  10
   Click element   ${terms}
   #Selenium2Library.Select Label    Xpath=//*[@id="authForm"]/div[2]/div[7]/label

press register button
   Wait Until Element is Visible    ${registerButton}  10
   Click element   ${registerButton}

Execute Manual Step    
   BuiltIn.Pass Execution    Please complete the CAPTCHA portion of the form.
   
check the confirmation message
   Wait Until Element is Visible    ${messageCreate}  10
    #Close browser

enter the referent code   
  Wait Until Element is Visible   ${refCode}  10
  Input text   ${refCode}   marisleidy19

press login button
  Wait Until Element is Visible    ${loginbutton}  10
  Click element   ${loginbutton}

press new account link
  Wait Until Element is Visible    ${registerAccountLink}  10
  Click element   ${registerAccountLink}
 
press start button
  Wait Until Element is Visible    ${startButton}  10
  Click element   ${startButton} 